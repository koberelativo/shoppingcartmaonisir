package com.example.acer.shoppingcart;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String dataFromScan;
    TextView tv;
    ListView lv;
    ArrayAdapter<String> kobe;
    ArrayList<String> list = new ArrayList<String>();
    double total = 0.0;
    int quantity;
    double price;
    String message = "";
    Button btnSend;
    Button cont;
    String number="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.total);
        lv = (ListView) findViewById(R.id.listView);

        kobe = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        lv.setAdapter(kobe);


        btnSend = (Button) findViewById(R.id.btnSend);
        cont = (Button) findViewById(R.id.contact);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

                startActivityForResult(intent,1);
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                message += "Total: " + total;
                if(total != 0) {
                    if(number!="") {
                        SmsManager sms = SmsManager.getDefault();
                        sms.sendTextMessage(number, null, message, null, null);
                        Toast.makeText(MainActivity.this, "SENT", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Set a contact", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(MainActivity.this, "Shopping list Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, Scanner.class);
        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if(data!=null) {
                dataFromScan = data.getStringExtra("cha");
               try {


                    String[] split = dataFromScan.split("\\|\\|\\|");
                   price = Double.parseDouble(split[2]);
                   quantity = Integer.parseInt(split[1]);
                   total += price * quantity;
                   String item = split[0].toString();
                    String temp = item + ", " + quantity + ", " + price;

                    list.add(temp);
                    kobe.notifyDataSetChanged();

                    message += temp + "\n";

                }
                catch(Exception e){
                    Toast.makeText(MainActivity.this, "Invalid data detected please scan another QR code", Toast.LENGTH_SHORT).show();
                }
            }

            tv.setText(total + "");
        }

        else if(requestCode == 1 && data!=null){
            Uri uri = data.getData();
            if(uri!=null ){
                Cursor c = null;
                try{
                    c = getContentResolver().query(uri, new String[]{
                                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                                    ContactsContract.CommonDataKinds.Phone.TYPE,
                                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                            null, null, null);
                    if (c != null && c.moveToFirst()) {
                        number = c.getString(0);

                        //showSelectedNumber(type, number);
                    }
                }finally{
                    if (c != null) {
                        c.close();
                    }
                }
            }

        }

    }
}